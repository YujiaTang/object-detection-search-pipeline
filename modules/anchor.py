import math
import torch
import torch.nn as nn

"""
TODO: normalized and denormalized
"""
class RetinaAnchors(nn.Module):
    """
    area: scales for different pyramid levels(anchor)
    stride:
    feature_size:
    """
    def __init__(self,
                 areas=torch.tensor([[32, 32], [64, 64], [128, 128], [256, 256],
                          [512, 512]]),
                 ratios=torch.tensor([0.5, 1, 2]),
                 scales=torch.tensor([2**0, 2**(1.0 / 3.0), 2**(2.0 / 3.0)]),
                 strides=torch.tensor([8, 16, 32, 64, 128], dtype=torch.float)):
        """
        :param areas: scales for different pyramid levels
        :param ratios:
        :param scales:
        :param strides:
        """
        super(RetinaAnchors,self).__init__()
        self.areas = areas
        self.ratios = ratios
        self.scales = scales
        self.strides = strides

    def forward(self, batch_size, fpn_feature_sizes):
        """
                generate batch anchors
        """
        device = fpn_feature_sizes.get_device()
        one_sample_anchors = []
        for index, area in enumerate(self.areas):
            base_anchors = self.generate_base_anchors(area, self.scales, self.ratios)
            featrue_anchors = self.generate_anchors_on_feature_map(
                base_anchors, fpn_feature_sizes[index], self.strides[index])
            featrue_anchors = featrue_anchors.to(device)
            #append each feature maps' anchor. [[57600, 4],[14400, 4],[3600, 4],[900, 4],[225, 4]]
            one_sample_anchors.append(featrue_anchors)

        batch_anchors = []
        for per_level_featrue_anchors in one_sample_anchors:
            per_level_featrue_anchors = per_level_featrue_anchors.unsqueeze(
                0).repeat(batch_size, 1, 1)
            #[[B, 57600, 4], [B, 14400, 4], [B, 3600, 4], [B, 900, 4], [B, 225, 4]]
            batch_anchors.append(per_level_featrue_anchors)

        # if input size:[B,3,640,640]
        # batch_anchors shape:[[B, 57600, 4],[B, 14400, 4],[B, 3600, 4],[B, 900, 4],[B, 225, 4]]
        # per anchor format:[x_min,y_min,x_max,y_max]
        return batch_anchors

    def generate_base_anchors(self, area, scales, ratios):
        """
        [w,h]->[x_min,y_min,x_max,y_max]
        """
        #get 9 types of scale. Multiple with feature size to get real sizes, 9*2
        aspects = torch.tensor([[[s * math.sqrt(r), s * math.sqrt(1 / r)]
                                 for s in scales]
                                for r in ratios]).view(-1, 2).float()
        #9*4
        area = torch.as_tensor(area, dtype=torch.float32)
        #print(aspects.dtype)
        #print(area.dtype)
        base_anchors = torch.zeros(len(scales)*len(ratios),4)
        #real width and height on feature map
        base_w_h = (area * aspects).float()
        #print(base_w_h.dtype)
        base_anchors[:, 2:] += base_w_h

        #convert base_anchors format:  [x_min,y_min,x_max,y_max]
        base_anchors[:, 0] -= base_anchors[:, 2] / 2
        base_anchors[:, 1] -= base_anchors[:, 3] / 2
        base_anchors[:, 2] /= 2
        base_anchors[:, 3] /= 2
        #[9,4]
        return base_anchors

    def generate_anchors_on_feature_map(self, base_anchors, feature_map_size, stride):
        """
        :param base_anchors:
        :return:
        """
        #[0,0][0,1][0,3].......
        #feature_size = image_size / stride
        shifts_x = (torch.arange(0, feature_map_size[0]) + 0.5) * stride
        shifts_y = (torch.arange(0, feature_map_size[1]) + 0.5) * stride

        # shifts shape:[w,h,2] -> (repeat) [w,h,4] -> (unsqueeze) [w,h,1,4]
        #------------------------[0,0,0,0][0,1,0,1]------------------------
        shifts = torch.tensor([[[shift_x, shift_y] for shift_y in shifts_y]
                               for shift_x in shifts_x]).repeat(1, 1,
                                                                2).unsqueeze(2).float()

        # base anchors shape:[9,4] -> [1,1,9,4],expand dimension
        base_anchors = base_anchors.unsqueeze(0).unsqueeze(0).float()
        # generate all featrue map anchors on each feature map points
        # featrue map anchors shape:[w,h,9,4] -> [h,w,9,4] -> [h*w*9,4]
        #print(type(base_anchors))
        #print(type(shifts))
        feature_map_anchors = (base_anchors + shifts).permute(
            1, 0, 2, 3).contiguous().view(-1, 4)

        # feature_map_anchors format: [anchor_nums,4],4:[x_min,y_min,x_max,y_max] [h*w*9,4]
        return feature_map_anchors