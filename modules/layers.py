# ProxylessNAS: Direct Neural Architecture Search on Target Task and Hardware
# Han Cai, Ligeng Zhu, Song Han
# International Conference on Learning Representations (ICLR), 2019.

from utils import *
from collections import OrderedDict
import torch.nn.functional as F
import numpy as np
def set_layer_from_config(layer_config):
    if layer_config is None:
        return None

    name2layer = {
        ConvLayer.__name__: ConvLayer,
        DepthConvLayer.__name__: DepthConvLayer,
        PoolingLayer.__name__: PoolingLayer,
        IdentityLayer.__name__: IdentityLayer,
        LinearLayer.__name__: LinearLayer,
        MBInvertedConvLayer.__name__: MBInvertedConvLayer,
        ZeroLayer.__name__: ZeroLayer,
        ResConvLayer.__name__: ResConvLayer,
        FPN.__name__: FPN,
        RegressionHead.__name__: RegressionHead,
        ClassificationHead.__name__: ClassificationHead,
    }

    layer_name = layer_config.pop('name')
    layer = name2layer[layer_name]
    return layer.build_from_config(layer_config)


class My2DLayer(MyModule):

    def __init__(self, in_channels, out_channels,
                 use_bn=True, act_func='relu', dropout_rate=0, ops_order='weight_bn_act'):
        super(My2DLayer, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels

        self.use_bn = use_bn
        self.act_func = act_func
        self.dropout_rate = dropout_rate
        self.ops_order = ops_order

        """ modules """
        modules = {}
        # batch norm
        if self.use_bn:
            if self.bn_before_weight:
                modules['bn'] = nn.BatchNorm2d(in_channels)
            else:
                modules['bn'] = nn.BatchNorm2d(out_channels)
        else:
            modules['bn'] = None
        # activation
        modules['act'] = build_activation(self.act_func, self.ops_list[0] != 'act')
        # dropout
        if self.dropout_rate > 0:
            modules['dropout'] = nn.Dropout2d(self.dropout_rate, inplace=True)
        else:
            modules['dropout'] = None
        # weight
        modules['weight'] = self.weight_op()

        # add modules
        for op in self.ops_list:
            if modules[op] is None:
                continue
            elif op == 'weight':
                if modules['dropout'] is not None:
                    self.add_module('dropout', modules['dropout'])
                for key in modules['weight']:
                    self.add_module(key, modules['weight'][key])
            else:
                self.add_module(op, modules[op])

    @property
    def ops_list(self):
        return self.ops_order.split('_')

    @property
    def bn_before_weight(self):
        for op in self.ops_list:
            if op == 'bn':
                return True
            elif op == 'weight':
                return False
        raise ValueError('Invalid ops_order: %s' % self.ops_order)

    def weight_op(self):
        raise NotImplementedError

    """ Methods defined in MyModule """

    def forward(self, x):
        for module in self._modules.values():
            x = module(x)
        return x

    @property
    def module_str(self):
        raise NotImplementedError

    @property
    def config(self):
        return {
            'in_channels': self.in_channels,
            'out_channels': self.out_channels,
            'use_bn': self.use_bn,
            'act_func': self.act_func,
            'dropout_rate': self.dropout_rate,
            'ops_order': self.ops_order,
        }

    @staticmethod
    def build_from_config(config):
        raise NotImplementedError

    def get_flops(self, x):
        raise NotImplementedError

    @staticmethod
    def is_zero_layer():
        return False


class ConvLayer(My2DLayer):

    def __init__(self, in_channels, out_channels,
                 kernel_size=3, stride=1, dilation=1, groups=1, bias=False, has_shuffle=False,
                 use_bn=True, act_func='relu', dropout_rate=0, ops_order='weight_bn_act'):
        self.kernel_size = kernel_size
        self.stride = stride
        self.dilation = dilation
        self.groups = groups
        self.bias = bias
        self.has_shuffle = has_shuffle

        super(ConvLayer, self).__init__(in_channels, out_channels, use_bn, act_func, dropout_rate, ops_order)

    def weight_op(self):
        padding = get_same_padding(self.kernel_size)
        if isinstance(padding, int):
            padding *= self.dilation
        else:
            padding[0] *= self.dilation
            padding[1] *= self.dilation

        weight_dict = OrderedDict()
        weight_dict['conv'] = nn.Conv2d(
            self.in_channels, self.out_channels, kernel_size=self.kernel_size, stride=self.stride, padding=padding,
            dilation=self.dilation, groups=self.groups, bias=self.bias
        )
        if self.has_shuffle and self.groups > 1:
            weight_dict['shuffle'] = ShuffleLayer(self.groups)

        return weight_dict

    @property
    def module_str(self):
        if isinstance(self.kernel_size, int):
            kernel_size = (self.kernel_size, self.kernel_size)
        else:
            kernel_size = self.kernel_size
        if self.groups == 1:
            if self.dilation > 1:
                return '%dx%d_DilatedConv' % (kernel_size[0], kernel_size[1])
            else:
                return '%dx%d_Conv' % (kernel_size[0], kernel_size[1])
        else:
            if self.dilation > 1:
                return '%dx%d_DilatedGroupConv' % (kernel_size[0], kernel_size[1])
            else:
                return '%dx%d_GroupConv' % (kernel_size[0], kernel_size[1])

    @property
    def config(self):
        return {
            'name': ConvLayer.__name__,
            'kernel_size': self.kernel_size,
            'stride': self.stride,
            'dilation': self.dilation,
            'groups': self.groups,
            'bias': self.bias,
            'has_shuffle': self.has_shuffle,
            **super(ConvLayer, self).config,
        }

    @staticmethod
    def build_from_config(config):
        return ConvLayer(**config)

    def get_flops(self, x):
        return count_conv_flop(self.conv, x), self.forward(x)


class DepthConvLayer(My2DLayer):

    def __init__(self, in_channels, out_channels,
                 kernel_size=3, stride=1, dilation=1, groups=1, bias=False, has_shuffle=False,
                 use_bn=True, act_func='relu', dropout_rate=0, ops_order='weight_bn_act'):
        self.kernel_size = kernel_size
        self.stride = stride
        self.dilation = dilation
        self.groups = groups
        self.bias = bias
        self.has_shuffle = has_shuffle

        super(DepthConvLayer, self).__init__(
            in_channels, out_channels, use_bn, act_func, dropout_rate, ops_order
        )

    def weight_op(self):
        padding = get_same_padding(self.kernel_size)
        if isinstance(padding, int):
            padding *= self.dilation
        else:
            padding[0] *= self.dilation
            padding[1] *= self.dilation

        weight_dict = OrderedDict()
        weight_dict['depth_conv'] = nn.Conv2d(
            self.in_channels, self.in_channels, kernel_size=self.kernel_size, stride=self.stride, padding=padding,
            dilation=self.dilation, groups=self.in_channels, bias=False
        )
        weight_dict['point_conv'] = nn.Conv2d(
            self.in_channels, self.out_channels, kernel_size=1, groups=self.groups, bias=self.bias
        )
        if self.has_shuffle and self.groups > 1:
            weight_dict['shuffle'] = ShuffleLayer(self.groups)
        return weight_dict

    @property
    def module_str(self):
        if isinstance(self.kernel_size, int):
            kernel_size = (self.kernel_size, self.kernel_size)
        else:
            kernel_size = self.kernel_size
        if self.dilation > 1:
            return '%dx%d_DilatedDepthConv' % (kernel_size[0], kernel_size[1])
        else:
            return '%dx%d_DepthConv' % (kernel_size[0], kernel_size[1])

    @property
    def config(self):
        return {
            'name': DepthConvLayer.__name__,
            'kernel_size': self.kernel_size,
            'stride': self.stride,
            'dilation': self.dilation,
            'groups': self.groups,
            'bias': self.bias,
            'has_shuffle': self.has_shuffle,
            **super(DepthConvLayer, self).config,
        }

    @staticmethod
    def build_from_config(config):
        return DepthConvLayer(**config)

    def get_flops(self, x):
        depth_flop = count_conv_flop(self.depth_conv, x)
        x = self.depth_conv(x)
        point_flop = count_conv_flop(self.point_conv, x)
        x = self.point_conv(x)
        return depth_flop + point_flop, self.forward(x)


class PoolingLayer(My2DLayer):

    def __init__(self, in_channels, out_channels,
                 pool_type, kernel_size=2, stride=2,
                 use_bn=False, act_func=None, dropout_rate=0, ops_order='weight_bn_act'):
        self.pool_type = pool_type
        self.kernel_size = kernel_size
        self.stride = stride

        super(PoolingLayer, self).__init__(in_channels, out_channels, use_bn, act_func, dropout_rate, ops_order)

    def weight_op(self):
        #if self.stride == 1:
            # same padding if `stride == 1`
        padding = get_same_padding(self.kernel_size)
        #else:
            #padding = 0

        weight_dict = OrderedDict()
        if self.pool_type == 'avg':
            weight_dict['pool'] = nn.AvgPool2d(
                self.kernel_size, stride=self.stride, padding=padding, count_include_pad=False
            )
        elif self.pool_type == 'max':
            weight_dict['pool'] = nn.MaxPool2d(self.kernel_size, stride=self.stride, padding=padding)
        else:
            raise NotImplementedError
        return weight_dict

    @property
    def module_str(self):
        if isinstance(self.kernel_size, int):
            kernel_size = (self.kernel_size, self.kernel_size)
        else:
            kernel_size = self.kernel_size
        return '%dx%d_%sPool' % (kernel_size[0], kernel_size[1], self.pool_type.upper())

    @property
    def config(self):
        return {
            'name': PoolingLayer.__name__,
            'pool_type': self.pool_type,
            'kernel_size': self.kernel_size,
            'stride': self.stride,
            **super(PoolingLayer, self).config
        }

    @staticmethod
    def build_from_config(config):
        return PoolingLayer(**config)

    def get_flops(self, x):
        return 0, self.forward(x)


class IdentityLayer(My2DLayer):

    def __init__(self, in_channels, out_channels,
                 use_bn=False, act_func=None, dropout_rate=0, ops_order='weight_bn_act'):
        super(IdentityLayer, self).__init__(in_channels, out_channels, use_bn, act_func, dropout_rate, ops_order)

    def weight_op(self):
        return None

    @property
    def module_str(self):
        return 'Identity'

    @property
    def config(self):
        return {
            'name': IdentityLayer.__name__,
            **super(IdentityLayer, self).config,
        }

    @staticmethod
    def build_from_config(config):
        return IdentityLayer(**config)

    def get_flops(self, x):
        return 0, self.forward(x)


class LinearLayer(MyModule):

    def __init__(self, in_features, out_features, bias=True,
                 use_bn=False, act_func=None, dropout_rate=0, ops_order='weight_bn_act'):
        super(LinearLayer, self).__init__()

        self.in_features = in_features
        self.out_features = out_features
        self.bias = bias

        self.use_bn = use_bn
        self.act_func = act_func
        self.dropout_rate = dropout_rate
        self.ops_order = ops_order

        """ modules """
        modules = {}
        # batch norm
        if self.use_bn:
            if self.bn_before_weight:
                modules['bn'] = nn.BatchNorm1d(in_features)
            else:
                modules['bn'] = nn.BatchNorm1d(out_features)
        else:
            modules['bn'] = None
        # activation
        modules['act'] = build_activation(self.act_func, self.ops_list[0] != 'act')
        # dropout
        if self.dropout_rate > 0:
            modules['dropout'] = nn.Dropout(self.dropout_rate, inplace=True)
        else:
            modules['dropout'] = None
        # linear
        modules['weight'] = {'linear': nn.Linear(self.in_features, self.out_features, self.bias)}

        # add modules
        for op in self.ops_list:
            if modules[op] is None:
                continue
            elif op == 'weight':
                if modules['dropout'] is not None:
                    self.add_module('dropout', modules['dropout'])
                for key in modules['weight']:
                    self.add_module(key, modules['weight'][key])
            else:
                self.add_module(op, modules[op])

    @property
    def ops_list(self):
        return self.ops_order.split('_')

    @property
    def bn_before_weight(self):
        for op in self.ops_list:
            if op == 'bn':
                return True
            elif op == 'weight':
                return False
        raise ValueError('Invalid ops_order: %s' % self.ops_order)

    def forward(self, x):
        for module in self._modules.values():
            x = module(x)
        return x

    @property
    def module_str(self):
        return '%dx%d_Linear' % (self.in_features, self.out_features)

    @property
    def config(self):
        return {
            'name': LinearLayer.__name__,
            'in_features': self.in_features,
            'out_features': self.out_features,
            'bias': self.bias,
            'use_bn': self.use_bn,
            'act_func': self.act_func,
            'dropout_rate': self.dropout_rate,
            'ops_order': self.ops_order,
        }

    @staticmethod
    def build_from_config(config):
        return LinearLayer(**config)

    def get_flops(self, x):
        return self.linear.weight.numel(), self.forward(x)

    @staticmethod
    def is_zero_layer():
        return False


class MBInvertedConvLayer(MyModule):

    def __init__(self, in_channels, out_channels,
                 kernel_size=3, stride=1, expand_ratio=6, mid_channels=None):
        super(MBInvertedConvLayer, self).__init__()

        self.in_channels = in_channels
        self.out_channels = out_channels

        self.kernel_size = kernel_size
        self.stride = stride
        self.expand_ratio = expand_ratio
        self.mid_channels = mid_channels

        if self.mid_channels is None:
            feature_dim = round(self.in_channels * self.expand_ratio)
        else:
            feature_dim = self.mid_channels

        if self.expand_ratio == 1:
            self.inverted_bottleneck = None
        else:
            self.inverted_bottleneck = nn.Sequential(OrderedDict([
                ('conv', nn.Conv2d(self.in_channels, feature_dim, 1, 1, 0, bias=False)),
                ('bn', nn.BatchNorm2d(feature_dim)),
                ('act', nn.ReLU6(inplace=True)),
            ]))

        pad = get_same_padding(self.kernel_size)
        self.depth_conv = nn.Sequential(OrderedDict([
            ('conv', nn.Conv2d(feature_dim, feature_dim, kernel_size, stride, pad, groups=feature_dim, bias=False)),
            ('bn', nn.BatchNorm2d(feature_dim)),
            ('act', nn.ReLU6(inplace=True)),
        ]))

        self.point_linear = nn.Sequential(OrderedDict([
            ('conv', nn.Conv2d(feature_dim, out_channels, 1, 1, 0, bias=False)),
            ('bn', nn.BatchNorm2d(out_channels)),
        ]))

    def forward(self, x):
        if self.inverted_bottleneck:
            x = self.inverted_bottleneck(x)
        x = self.depth_conv(x)
        x = self.point_linear(x)
        return x

    @property
    def module_str(self):
        return '%dx%d_MBConv%d' % (self.kernel_size, self.kernel_size, self.expand_ratio)

    @property
    def config(self):
        return {
            'name': MBInvertedConvLayer.__name__,
            'in_channels': self.in_channels,
            'out_channels': self.out_channels,
            'kernel_size': self.kernel_size,
            'stride': self.stride,
            'expand_ratio': self.expand_ratio,
            'mid_channels': self.mid_channels,
        }

    @staticmethod
    def build_from_config(config):
        return MBInvertedConvLayer(**config)

    def get_flops(self, x):
        if self.inverted_bottleneck:
            flop1 = count_conv_flop(self.inverted_bottleneck.conv, x)
            x = self.inverted_bottleneck(x)
        else:
            flop1 = 0

        flop2 = count_conv_flop(self.depth_conv.conv, x)
        x = self.depth_conv(x)

        flop3 = count_conv_flop(self.point_linear.conv, x)
        x = self.point_linear(x)

        return flop1 + flop2 + flop3, x

    @staticmethod
    def is_zero_layer():
        return False

class ResConvLayer(MyModule):
    def __init__(self, in_channels, out_channels,
                 kernel_size=3,stride=1,expand_ratio=4):
        super(ResConvLayer, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels

        self.kernel_size = kernel_size
        self.stride = stride
        self.expand_ratio = expand_ratio
        #if self.mid_channels is None:
        #    feature_dim = round(self.in_channels * self.expand_ratio)
        #else:
        #    feature_dim = self.mid_channels


        if self.in_channels != self.out_channels:
            if self.stride !=1:
                self.inner = self.in_channels//2
                self.conv1 = nn.Sequential(OrderedDict([
                   ('conv',nn.Conv2d(self.in_channels, self.inner, kernel_size=1,
                                     stride=1, bias=False)),
                    ('bn',nn.BatchNorm2d(self.inner)),
                    ('act', nn.ReLU(inplace=True))
                ]))
            else:
                self.inner = self.in_channels
                self.conv1 = nn.Sequential(OrderedDict([
                    ('conv', nn.Conv2d(self.in_channels, self.inner, kernel_size=1,
                                     stride=1, bias=False)),
                    ('bn', nn.BatchNorm2d(self.in_channels)),
                    ('act', nn.ReLU(inplace=True))
                ]))
            self.shortcut = nn.Sequential(OrderedDict([
                ('conv', nn.Conv2d(self.in_channels, self.out_channels,
                          kernel_size=1, stride=self.stride, bias=False)),
                ('bn', nn.BatchNorm2d(self.out_channels))
            ]))
        else:
            self.inner = self.in_channels//self.expand_ratio
            self.conv1 = nn.Sequential(OrderedDict([
                ('conv', nn.Conv2d(self.in_channels, self.inner, kernel_size=1,
                                   stride=1, bias=False)),
                ('bn', nn.BatchNorm2d(self.inner)),
                ('act', nn.ReLU(inplace=True)),
            ]))
            self.shortcut = nn.Sequential()
        padding = get_same_padding(self.kernel_size)
        self.conv2 = nn.Sequential(OrderedDict([
            ('conv', nn.Conv2d(self.inner, self.inner,
                               kernel_size=self.kernel_size, stride=self.stride, padding=padding, bias=False)),
            ('bn', nn.BatchNorm2d(self.inner)),
            ('act', nn.ReLU(inplace=True)),
        ]))
        self.conv3 = nn.Sequential(OrderedDict([
            ('conv', nn.Conv2d(self.inner, self.out_channels, kernel_size=1,
                               stride=1, bias=False)),
            ('bn', nn.BatchNorm2d(self.out_channels)),
            #('act', nn.ReLU(inplace=True)),
        ]))

    def forward(self, x):
        out = self.conv1(x)
        out = self.conv2(out)
        out = self.conv3(out)
        #print('test1')
        out = out + self.shortcut(x)
        #print('test2')
        out = F.relu(out)
        #print('test3')
        return out

    @property
    def module_str(self):
        return '%dx%d_ResConv%d' %(self.kernel_size, self.kernel_size, self.expand_ratio)

    @property
    def config(self):
        return{
            'name': ResConvLayer.__name__,
            'in_channels': self.in_channels,
            'out_channels': self.out_channels,
            'kernel_size': self.kernel_size,
            'stride': self.stride,
            'expand_ratio': self.expand_ratio,
            'mid_channels': self.mid_channels,
        }

    @staticmethod
    def build_from_config(config):
        return ResConvLayer(**config)

    def get_flops(self, x):
        flop1 = count_conv_flop(self.conv1.conv, x)
        out = self.conv1(x)
        flop2 = count_conv_flop(self.conv2.conv, out)
        out = self.conv2(out)
        flop3 = count_conv_flop(self.conv3.conv, out)
        out = self.conv3(out)
        if self.in_channels != self.out_channels:
            flop4 = count_conv_flop(self.shortcut.conv, x)
        else:
            flop4 = 0
        #print('x', x.shape)
        #print('out', out.shape)
        out = out + self.shortcut(x)
        out = F.relu(out)
        flops = flop1 + flop2 + flop3 + flop4
        return flops, out

    @staticmethod
    def is_zero_layer():
        return False

class ZeroLayer(MyModule):

    def __init__(self, stride):
        super(ZeroLayer, self).__init__()
        self.stride = stride

    def forward(self, x):
        n, c, h, w = x.size()
        h //= self.stride
        w //= self.stride
        device = x.get_device() if x.is_cuda else torch.device('cpu')
        # noinspection PyUnresolvedReferences
        padding = torch.zeros(n, c, h, w, device=device, requires_grad=False)
        return padding

    @property
    def module_str(self):
        return 'Zero'

    @property
    def config(self):
        return {
            'name': ZeroLayer.__name__,
            'stride': self.stride,
        }

    @staticmethod
    def build_from_config(config):
        return ZeroLayer(**config)

    def get_flops(self, x):
        return 0, self.forward(x)

    @staticmethod
    def is_zero_layer():
        return True

class FPN(MyModule):
    def __init__(self, in_channels=[512, 1024, 2048], feature_channels=256):
        """
        :param in_channels: inchannels is a list[512, 1024, 2048]
        :param feature_channels: all featyre maps have the same num of channels(256)
        The functions of nn.Unsample and nn.functional.interpolate are the same, but nn.functional.interpolate
        can only be placed in the forward.
        """
        super(FPN,self).__init__()
        assert isinstance(in_channels, list)
        C3_size, C4_size, C5_size = in_channels[0], in_channels[1], in_channels[2]
        self.P5_1 = nn.Conv2d(C5_size, feature_channels, kernel_size=1, stride=1, padding=0)
        self.P5_upsampled = nn.Upsample(scale_factor=2, mode='nearest')
        self.P5_2 = nn.Conv2d(feature_channels, feature_channels, kernel_size=3, stride=1, padding=1)

        # add P5 elementwise to C4
        self.P4_1 = nn.Conv2d(C4_size, feature_channels, kernel_size=1, stride=1, padding=0)
        self.P4_upsampled = nn.Upsample(scale_factor=2, mode='nearest')
        self.P4_2 = nn.Conv2d(feature_channels, feature_channels, kernel_size=3, stride=1, padding=1)

        # add P4 elementwise to C3
        self.P3_1 = nn.Conv2d(C3_size, feature_channels, kernel_size=1, stride=1, padding=0)
        self.P3_2 = nn.Conv2d(feature_channels, feature_channels, kernel_size=3, stride=1, padding=1)

        # "P6 is obtained via a 3x3 stride-2 conv on C5"
        self.P6 = nn.Conv2d(C5_size, feature_channels, kernel_size=3, stride=2, padding=1)

        # "P7 is computed by applying ReLU followed by a 3x3 stride-2 conv on P6"
        self.P7_1 = nn.ReLU(inplace=True)
        self.P7_2 = nn.Conv2d(feature_channels, feature_channels, kernel_size=3, stride=2, padding=1)

    def forward(self, inputs):
        C3, C4, C5 = inputs
        np.set_printoptions(threshold=np.inf)
        #print('C3 SHAPE', C3.shape)
        #print('C3', C3)
        #print('C4 SHAPE', C4.shape)
        #print('C4', C4)
        #print('C5 SHAPE', C5.shape)
        #print('C5', C5)
        P5_x = self.P5_1(C5)
        P5_upsampled_x = self.P5_upsampled(P5_x)
        P5_x = self.P5_2(P5_x)

        P4_x = self.P4_1(C4)
        P4_x = P5_upsampled_x + P4_x
        P4_upsampled_x = self.P4_upsampled(P4_x)
        P4_x = self.P4_2(P4_x)

        P3_x = self.P3_1(C3)
        P3_x = P3_x + P4_upsampled_x
        P3_x = self.P3_2(P3_x)

        P6_x = self.P6(C5)

        P7_x = self.P7_1(P6_x)
        P7_x = self.P7_2(P7_x)

        return [P3_x, P4_x, P5_x, P6_x, P7_x]

    @property
    def module_str(self):
        return '%d_layers_FPN%d' % (len(self.in_channels)+2, self.feature_channels)

    @property
    def config(self):
        return {
            'name': FPN.__name__,
            'in_channels': self.in_channels,
            'feature_channels': self.feature_channels,
        }

    @staticmethod
    def build_from_config(config):
        return FPN(**config)

    def get_flops(self, x):
        """
        TODO
        """
        raise NotImplementedError

    @staticmethod
    def is_zero_layer():
        return False

class RegressionHead(MyModule):
    """
    The initial strategy of heads is different!!!
    """
    def __init__(self,in_channels=256,feat_channels=256,num_anchors=9,stacked_convs=4):
        """
        :param in_channels: the num of input channels
        :param feat_channels: the num of out channels
        :param num_anchors: the num of anchors
        :param num_classes: the num of classes
        :param stacked_convs: the num of repetition(conv+act)
        nn.Squential: order
        nn.ModuleList: no order, iterable
        """
        super(RegressionHead, self).__init__()
        self.stacked_convs = stacked_convs
        self.in_channels = in_channels
        self.feat_chaneels = feat_channels
        self.num_anchors = num_anchors
        layers = []
        self.reg_convs = nn.Sequential()
        for i in range(self.stacked_convs):
            chn = self.in_channels if i==0 else self.feat_chaneels
            layers.append(
                nn.Conv2d(chn, self.feat_chaneels, kernel_size=3, stride=1, padding=1)
            )
            layers.append(nn.ReLU(inplace=True))
        self.reg_head = nn.Sequential(*layers)
        self.output = nn.Conv2d(self.feat_chaneels, self.num_anchors * 4, kernel_size=3, stride=1, padding=1)
        #for m in self.modules():
        #    if isinstance(m, nn.Conv2d):
        #        nn.init.normal_(m.weight, std=0.01)
        #        if m.bias is not None:
        #            nn.init.constant_(m.bias, val=0)

    def forward(self, x):
        """
        single feature map head
        """
        x = self.reg_head(x)
        x = self.output(x)
        # out is B x C x W x H, with C = 4*num_anchors
        out = x.permute(0,2,3,1)
        #print('reg out', out.shape)
        # B x W x H x C
        # B, W x H x num_anchors, 4
        return out.contiguous().view(out.shape[0], -1, 4)

    @property
    def module_str(self):
        return '%dstacked_RegressionHead' % (self.stacked_convs)

    @property
    def config(self):
        return {
            'name': RegressionHead.__name__,
            'in_channels': self.in_channels,
            'feat_channels': self.feat_channels,
            'stacked_convs': self.stacked_convs,
            'num_anchors': self.num_anchors,
        }

    @staticmethod
    def build_from_config(config):
        return FPN(**config)

    def get_flops(self, x):
        """
        TODO
        """
        raise NotImplementedError

    @staticmethod
    def is_zero_layer():
        return False

class ClassificationHead(MyModule):
    def __init__(self, in_channels=256,feat_channels=256,num_anchors=9,stacked_convs=4, num_classes=80, prior=0.01):
        """
        :param in_channels:
        :param feat_channels:
        :param num_anchors:
        :param stacked_convs:
        :param num_classes: the num of classes in dataset
        :param prior:
        """
        super(ClassificationHead, self).__init__()
        self.stacked_convs = stacked_convs
        self.in_channels = in_channels
        self.feat_chaneels = feat_channels
        self.num_anchors = num_anchors
        self.num_classes = num_classes
        layers = []
        for i in range(self.stacked_convs):
            chn = self.in_channels if i==0 else self.feat_chaneels
            layers.append(
                nn.Conv2d(chn, self.feat_chaneels, kernel_size=3, stride=1, padding=1)
            )
            layers.append(nn.ReLU(inplace=True))

        self.cls_head = nn.Sequential(*layers)
        self.output = nn.Conv2d(self.feat_chaneels, self.num_anchors * self.num_classes, kernel_size=3, stride=1,
                                padding=1)
        self.output_act = nn.Sigmoid()
        #for m in self.modules():
        #    if isinstance(m, nn.Conv2d):
        #        nn.init.normal_(m.weight, std=0.01)
        #        if m.bias is not None:
        #            nn.init.constant_(m.bias, val=0)

        #prior = prior
        #b = -math.log((1 - prior) / prior)
        #self.cls_head[-1].bias.data.fill_(b)


    def forward(self, x):
        """
        single feature map head
        """
        x = self.cls_head(x)
        x = self.output(x)
        out = self.output_act(x)
        #print('class out', out.shape)
        # out is B x C x W x H, with C = n_classes * n_anchors
        out1 = out.permute(0, 2, 3, 1)
        batch_size, width, height, channels = out1.shape
        out2 = out1.view(batch_size, width, height, self.num_anchors, self.num_classes)
        return out2.contiguous().view(x.shape[0], -1, self.num_classes)

    @property
    def module_str(self):
        return '%dstacked_ClassificationHead' % (self.stacked_convs)

    @property
    def config(self):
        return {
            'name': ClassificationHead.__name__,
            'in_channels': self.in_channels,
            'feat_channels': self.feat_channels,
            'stacked_convs': self.stacked_convs,
            'num_anchors': self.num_anchors,
        }

    @staticmethod
    def build_from_config(config):
        return FPN(**config)

    def get_flops(self, x):
        """
        TODO
        """
        raise NotImplementedError

    @staticmethod
    def is_zero_layer():
        return False