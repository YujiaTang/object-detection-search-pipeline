# ProxylessNAS: Direct Neural Architecture Search on Target Task and Hardware
# Han Cai, Ligeng Zhu, Song Han
# International Conference on Learning Representations (ICLR), 2019.

from modules.layers import *
import json
from modules.anchor import *
from modules.loss import *
#import numpy as np


def proxyless_base(net_config=None, n_classes=1000, bn_param=(0.1, 1e-3), dropout_rate=0):
    assert net_config is not None, 'Please input a network config'
    net_config_path = download_url(net_config)
    net_config_json = json.load(open(net_config_path, 'r'))

    net_config_json['classifier']['out_features'] = n_classes
    net_config_json['classifier']['dropout_rate'] = dropout_rate

    net = ProxylessNASNets.build_from_config(net_config_json)
    net.set_bn_param(momentum=bn_param[0], eps=bn_param[1])

    return net


class MobileInvertedResidualBlock(MyModule):

    def __init__(self, mobile_inverted_conv, shortcut=None):
        super(MobileInvertedResidualBlock, self).__init__()

        self.mobile_inverted_conv = mobile_inverted_conv
        self.shortcut = shortcut

    def forward(self, x):
        if self.mobile_inverted_conv.is_zero_layer(): #zero layer
            res = x
        elif self.shortcut is None or self.shortcut.is_zero_layer(): #not first layer
            res = self.mobile_inverted_conv(x)
        #else:
        #    conv_x = self.mobile_inverted_conv(x) #has shortcut identity
        #    skip_x = self.shortcut(x)
        #    res = skip_x + conv_x
        return res

    @property
    def module_str(self):
        return '(%s)' % (self.mobile_inverted_conv.module_str)

    @property
    def config(self):
        return {
            'name': MobileInvertedResidualBlock.__name__,
            'mobile_inverted_conv': self.mobile_inverted_conv.config,
            'shortcut': self.shortcut.config if self.shortcut is not None else None,
        }

    @staticmethod
    def build_from_config(config):
        mobile_inverted_conv = set_layer_from_config(config['mobile_inverted_conv'])
        shortcut = set_layer_from_config(config['shortcut'])
        return MobileInvertedResidualBlock(mobile_inverted_conv, shortcut)

    def get_flops(self, x):
        flops1, conv_x = self.mobile_inverted_conv.get_flops(x)
        if self.shortcut:
            flops2, _ = self.shortcut.get_flops(x)
        else:
            flops2 = 0

        return flops1 + flops2, self.forward(x)


class ProxylessNASNets(MyNetwork):

    def __init__(self, first_conv, second_conv, blocks, fpn, cls_head, reg_head, anchors):
        super(ProxylessNASNets, self).__init__()

        self.first_conv = first_conv
        self.second_conv = second_conv
        self.blocks = nn.ModuleList(blocks)
        self.fpn = fpn
        self.cls_head = cls_head
        self.reg_head = reg_head
        self.anchors = anchors
        #self.feature_mix_layer = feature_mix_layer
        #self.global_avg_pooling = nn.AdaptiveAvgPool2d(1)
        #self.classifier = classifier
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

        prior = 0.01

        self.cls_head.output.weight.data.fill_(0)
        self.cls_head.output.bias.data.fill_(-math.log((1.0 - prior) / prior))

        self.reg_head.output.weight.data.fill_(0)
        self.reg_head.output.bias.data.fill_(0)

    def forward(self, x):
        self.batch_size, _, _, _ = x.shape
        #print('x', x.shape)
        device = x.get_device()
        inputs = []
        x = self.first_conv(x)
        #print('x1',x.shape)
        x = self.second_conv(x)
        #print('x2', x.shape)
        i=1
        for block in self.blocks:
            x = block(x)
            #print(x.shape)
            if i==10 or i ==20 or i==25:
                inputs.append(x)
            i += 1
        feature_maps = self.fpn(inputs)
        self.fpn_feature_sizes = []
        cls_heads, reg_heads = [], []
        # [N, 9*4,H,W] -> [N,H*W*9, 4]
        for feature in feature_maps:
            #print('feature', feature.shape)
            self.fpn_feature_sizes.append([feature.shape[3], feature.shape[2]])
            cls_head = self.cls_head(feature)
            #print('cls_head shape', cls_head.shape)
            #np.set_printoptions(threshold=np.inf)
            #cls_head_cpu=cls_head.cpu()
            #print('cls_head', cls_head_cpu.detach().numpy())
            #print(cls_head.shape)
            # [N,9*num_classes,H,W] -> [N,H*W*9,num_classes]
            cls_heads.append(cls_head)
            reg_head = self.reg_head(feature)
            #print('reg_head shape', reg_head.shape)
            #print('reg_head', reg_head)
            #print(reg_head.shape)
            # [N, 9*4,H,W] -> [N,H*W*9, 4]
            reg_heads.append(reg_head)
        # if input size:[B,3,640,640]
        # features shape:[[B, 256, 80, 80],[B, 256, 40, 40],[B, 256, 20, 20],[B, 256, 10, 10],[B, 256, 5, 5]]
        # cls_heads shape:[[B, 57600, 80],[B, 14400, 80],[B, 3600, 80],[B, 900, 80],[B, 225, 80]]
        # reg_heads shape:[[B, 57600, 4],[B, 14400, 4],[B, 3600, 4],[B, 900, 4],[B, 225, 4]]
        # batch_anchors shape:[[B, 57600, 4],[B, 14400, 4],[B, 3600, 4],[B, 900, 4],[B, 225, 4]]
        self.fpn_feature_sizes = torch.tensor(self.fpn_feature_sizes).to(device)
        #x = self.feature_mix_layer(x)
        #x = self.global_avg_pooling(x)
        #x = x.view(x.size(0), -1)  # flatten
        #x = self.classifier(x)
        batch_anchors = self.anchors(self.batch_size, self.fpn_feature_sizes)
        return cls_heads, reg_heads, batch_anchors

    @property
    def module_str(self):
        _str = ''
        for block in self.blocks:
            _str += block.unit_str + '\n'
        return _str

    @property
    def config(self):
        return {
            'name': ProxylessNASNets.__name__,
            'bn': self.get_bn_param(),
            'first_conv': self.first_conv.config,
            'second_conv': self.second_conv.config,
            'blocks': [
                block.config for block in self.blocks
            ],
            #'feature_mix_layer': self.feature_mix_layer.config,
            'classifier': self.classifier.config,
        }

    @staticmethod
    def build_from_config(config):
        first_conv = set_layer_from_config(config['first_conv'])
        second_conv = set_layer_from_config(config['second_conv'])
        #feature_mix_layer = set_layer_from_config(config['feature_mix_layer'])
        classifier = set_layer_from_config(config['classifier'])
        blocks = []
        for block_config in config['blocks']:
            blocks.append(MobileInvertedResidualBlock.build_from_config(block_config))

        net = ProxylessNASNets(first_conv, second_conv, blocks, feature_mix_layer, classifier)
        if 'bn' in config:
            net.set_bn_param(**config['bn'])
        else:
            net.set_bn_param(momentum=0.1, eps=1e-3)

        return net

    def get_flops(self, x):
        flop, x = self.first_conv.get_flops(x)
        delta_flop, x = self.second_conv.get_flops(x)
        flop += delta_flop
        for block in self.blocks:
            delta_flop, x = block.get_flops(x)
            flop += delta_flop

        #delta_flop, x = self.feature_mix_layer.get_flops(x)
        #flop += delta_flop

        #x = self.global_avg_pooling(x)
        #x = x.view(x.size(0), -1)  # flatten

        #delta_flop, x = self.classifier.get_flops(x)
        #flop += delta_flop
        return flop, x
