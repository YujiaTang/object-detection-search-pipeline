import os
import cv2
import torch
import numpy as np
import random
import math
from torch.utils.data import Dataset
from pycocotools.coco import COCO
import torchvision.transforms as transforms
#import torch.nn.functional as F

COCO_CLASSES = [
    "person",
    "bicycle",
    "car",
    "motorcycle",
    "airplane",
    "bus",
    "train",
    "truck",
    "boat",
    "traffic light",
    "fire hydrant",
    "stop sign",
    "parking meter",
    "bench",
    "bird",
    "cat",
    "dog",
    "horse",
    "sheep",
    "cow",
    "elephant",
    "bear",
    "zebra",
    "giraffe",
    "backpack",
    "umbrella",
    "handbag",
    "tie",
    "suitcase",
    "frisbee",
    "skis",
    "snowboard",
    "sports ball",
    "kite",
    "baseball bat",
    "baseball glove",
    "skateboard",
    "surfboard",
    "tennis racket",
    "bottle",
    "wine glass",
    "cup",
    "fork",
    "knife",
    "spoon",
    "bowl",
    "banana",
    "apple",
    "sandwich",
    "orange",
    "broccoli",
    "carrot",
    "hot dog",
    "pizza",
    "donut",
    "cake",
    "chair",
    "couch",
    "potted plant",
    "bed",
    "dining table",
    "toilet",
    "tv",
    "laptop",
    "mouse",
    "remote",
    "keyboard",
    "cell phone",
    "microwave",
    "oven",
    "toaster",
    "sink",
    "refrigerator",
    "book",
    "clock",
    "vase",
    "scissors",
    "teddy bear",
    "hair drier",
    "toothbrush",
]

colors = [
    (39, 129, 113),
    (164, 80, 133),
    (83, 122, 114),
    (99, 81, 172),
    (95, 56, 104),
    (37, 84, 86),
    (14, 89, 122),
    (80, 7, 65),
    (10, 102, 25),
    (90, 185, 109),
    (106, 110, 132),
    (169, 158, 85),
    (188, 185, 26),
    (103, 1, 17),
    (82, 144, 81),
    (92, 7, 184),
    (49, 81, 155),
    (179, 177, 69),
    (93, 187, 158),
    (13, 39, 73),
    (12, 50, 60),
    (16, 179, 33),
    (112, 69, 165),
    (15, 139, 63),
    (33, 191, 159),
    (182, 173, 32),
    (34, 113, 133),
    (90, 135, 34),
    (53, 34, 86),
    (141, 35, 190),
    (6, 171, 8),
    (118, 76, 112),
    (89, 60, 55),
    (15, 54, 88),
    (112, 75, 181),
    (42, 147, 38),
    (138, 52, 63),
    (128, 65, 149),
    (106, 103, 24),
    (168, 33, 45),
    (28, 136, 135),
    (86, 91, 108),
    (52, 11, 76),
    (142, 6, 189),
    (57, 81, 168),
    (55, 19, 148),
    (182, 101, 89),
    (44, 65, 179),
    (1, 33, 26),
    (122, 164, 26),
    (70, 63, 134),
    (137, 106, 82),
    (120, 118, 52),
    (129, 74, 42),
    (182, 147, 112),
    (22, 157, 50),
    (56, 50, 20),
    (2, 22, 177),
    (156, 100, 106),
    (21, 35, 42),
    (13, 8, 121),
    (142, 92, 28),
    (45, 118, 33),
    (105, 118, 30),
    (7, 185, 124),
    (46, 34, 146),
    (105, 184, 169),
    (22, 18, 5),
    (147, 71, 73),
    (181, 64, 91),
    (31, 39, 184),
    (164, 179, 33),
    (96, 50, 18),
    (95, 15, 106),
    (113, 68, 54),
    (136, 116, 112),
    (119, 139, 130),
    (31, 139, 34),
    (66, 6, 127),
    (62, 39, 2),
    (49, 99, 180),
    (49, 119, 155),
    (153, 50, 183),
    (125, 38, 3),
    (129, 87, 143),
    (49, 87, 40),
    (128, 62, 120),
    (73, 85, 148),
    (28, 144, 118),
    (29, 9, 24),
    (175, 45, 108),
    (81, 175, 64),
    (178, 19, 157),
    (74, 188, 190),
    (18, 114, 2),
    (62, 128, 96),
    (21, 3, 150),
    (0, 6, 95),
    (2, 20, 184),
    (122, 37, 185),
]


class CocoDetection(Dataset):
    def __init__(self,
                 image_root_dir='/mnt/nas2/CV/public_dataset/object_detection/COCO/',
                 annotation_root_dir='/mnt/nas2/CV/public_dataset/object_detection/COCO/annotations/',
                 set='train2017',
                 transform=None):
        self.image_root_dir = image_root_dir
        #self.annotation_root_dir = annotation_root_dir
        self.set_name = set
        self.transform = transform
        self.coco = COCO(annotation_root_dir)

        self.load_classes()

    def load_classes(self):
        # list total image id size: 118287
        self.image_ids = self.coco.getImgIds()
        print('image_id',len(self.image_ids))
        # list category: 80
        self.cat_ids = self.coco.getCatIds()
        # details of categories supercategory id name: 80
        self.categories = self.coco.loadCats(self.cat_ids)
        #sort
        self.categories.sort(key=lambda x: x['id'])

        # category_id is an original id,coco_id is set from 0 to 79
        # category['id']: i    i 0~79
        self.category_id_to_coco_label = {
            category['id']: i
            for i, category in enumerate(self.categories)
        }
        # v: k    v 0~79 k category['id']
        self.coco_label_to_category_id = {
            v: k
            for k, v in self.category_id_to_coco_label.items()
        }

    def __len__(self):
        return len(self.image_ids)

    def __getitem__(self, idx):
        img = self.load_image(idx)

        annot = self.load_annotations(idx)
        #if img is None:
            #return
        sample = {'img': img, 'annot': annot, 'scale': 1.}
        #if self.transform:
        sample = self.transform(sample)
        return sample


    def load_image(self, image_index):
        # detailed info of an image include: license, file_name, coco_url, height, width, date_captured……
        #94314: total training img
        image_info = self.coco.loadImgs(self.image_ids[image_index])[0]
        # get path of an image
        path = os.path.join(self.image_root_dir, image_info['file_name'])
        img = cv2.imread(path)
        #if type(img) == np.ndarray:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        return img.astype(np.float32) / 255.
        #else:
        #    return None

    def load_annotations(self, image_index):
        # get ground truth annotations in an image(id, not 0~79)
        annotations_ids = self.coco.getAnnIds(imgIds=self.image_ids[image_index], iscrowd=False)
        annotations = np.zeros((0, 5))

        # some images appear to miss annotations
        if len(annotations_ids) == 0:
            return annotations

        # parse annotations
        #get detailed info of annotations on an image include category index, id and coordinates
        coco_annotations = self.coco.loadAnns(annotations_ids)
        for _, a in enumerate(coco_annotations):
            # some annotations have basically no width / height, skip them
            if a['bbox'][2] < 1 or a['bbox'][3] < 1:
                continue
            # annotation [0, :4] coordinate [0, 4] category_id
            annotation = np.zeros((1, 5))
            annotation[0, :4] = a['bbox']
            annotation[0, 4] = self.find_coco_label_from_category_id(
                a['category_id'])

            annotations = np.append(annotations, annotation, axis=0)

        # transform from [x_min, y_min, w, h] to [x_min, y_min, x_max, y_max]
        annotations[:, 2] = annotations[:, 0] + annotations[:, 2]
        annotations[:, 3] = annotations[:, 1] + annotations[:, 3]

        return annotations

    def find_coco_label_from_category_id(self, category_id):
        return self.category_id_to_coco_label[category_id]

    def find_category_id_from_coco_label(self, coco_label):
        return self.coco_label_to_category_id[coco_label]

    def num_classes(self):
        return 80

    def image_aspect_ratio(self, image_index):
        image = self.coco.loadImgs(self.image_ids[image_index])[0]
        return float(image['width']) / float(image['height'])

class CoCoDataProvider(CocoDetection):

    def __init__(self, save_path=None, train_batch_size=32, test_batch_size=1, valid_size=5000,
                 n_worker=32):

        self._save_path = save_path
        #train_transforms = self.build_train_transform(distort_color, resize_scale)
        #train_dataset = datasets.ImageFolder(self.train_path, train_transforms)

        if valid_size is not None:
        #    if isinstance(valid_size, float):
        #        valid_size = int(valid_size * len(train_dataset))
        #    else:
        #        assert isinstance(valid_size, int), 'invalid valid_size: %s' % valid_size
        #    train_indexes, valid_indexes = self.random_sample_valid_set(
        #        [cls for _, cls in train_dataset.samples], valid_size, self.n_classes,
        #    )
        #    train_sampler = torch.utils.data.sampler.SubsetRandomSampler(train_indexes)
        #    valid_sampler = torch.utils.data.sampler.SubsetRandomSampler(valid_indexes)

        #    valid_dataset = datasets.ImageFolder(self.train_path, transforms.Compose([
        #        transforms.Resize(self.resize_value),
        #        transforms.CenterCrop(self.image_size),
        #        transforms.ToTensor(),
        #        self.normalize,
        #    ]))
            #valid_dataset =  MyDataset(self.train_path,   transforms.Compose([
            #    transforms.Resize(self.resize_value),
            #    transforms.CenterCrop(self.image_size),
            #   transforms.ToTensor(),
            #    self.normalize,
            #]))
            #self.train = torch.utils.data.DataLoader(
            #    train_dataset, batch_size=train_batch_size, sampler=train_sampler,
            #    num_workers=n_worker, pin_memory=True,
            #)
            train_dataset = CocoDetection(image_root_dir=self.train_path,
                                      annotation_root_dir=self.train_annotation_path,
                                      set="train2017",
                                      transform=transforms.Compose([
                                          RandomFlip(flip_prob=0.5),
                                          RandomCrop(crop_prob=0.5),
                                          RandomTranslate(translate_prob=0.5),
                                          Resize(resize=self.image_size),
                                      ]))
            self.train = torch.utils.data.DataLoader(train_dataset,
                                  batch_size=train_batch_size,
                                  shuffle=True,
                                  num_workers=n_worker,
                                  collate_fn=collater)
            val_dataset = CocoDetection(image_root_dir=self.valid_path,
                                    annotation_root_dir=self.valid_annotation_path,
                                    set="val2017",
                                    transform=transforms.Compose([
                                        Resize(resize=self.image_size),
                                    ]))
            self.valid = torch.utils.data.DataLoader(val_dataset,
                                batch_size=valid_size,
                                shuffle=True,
                                num_workers=n_worker,
                                collate_fn=collater)
            #self.valid = torch.utils.data.DataLoader(
            #    valid_dataset, batch_size=test_batch_size, sampler=valid_sampler,
            #    num_workers=n_worker, pin_memory=True,
            #)

            self.test_dataset = CocoDetection(image_root_dir=self.test_path,
                                        annotation_root_dir=self.test_annotation_path,
                                        set="val2017",
                                        transform=transforms.Compose([
                                            Resize(resize=self.image_size),
                                        ]))
            self.test = torch.utils.data.DataLoader(self.test_dataset,
                                                     batch_size=test_batch_size,
                                                    shuffle=True,
                                                    num_workers=n_worker,
                                                    collate_fn=collater)
        #self.test = torch.utils.data.DataLoader(
        #    MyDataset(self.valid_path, transforms.Compose([
        #        transforms.Resize(self.resize_value),
        #        transforms.CenterCrop(self.image_size),
        #        transforms.ToTensor(),
        #        self.normalize,
        #    ])), batch_size=test_batch_size, shuffle=False, num_workers=n_worker, pin_memory=True,
        #)


    @staticmethod
    def name():
        return 'coco'

    @property
    def data_shape(self):
        return 3, self.image_size, self.image_size  # C, H, W

    @property
    def n_classes(self):
        return 80

    @property
    def save_path(self):
        if self._save_path is None:
            self._save_path = '/mnt/nas4/CV/public_dataset/object_detection/COCO/'
        return self._save_path

    @property
    def data_url(self):
        raise ValueError('unable to download ImageNet')

    @property
    def train_path(self):
        return os.path.join(self.save_path, 'images/train2017/')

    @property
    def valid_path(self):
        return os.path.join(self.save_path, 'images/train2017/')

    @property
    def test_path(self):
        return os.path.join(self.save_path, 'val2017/')

    @property
    def train_annotation_path(self):
        return os.path.join('/mnt/nas7/users/guowen/aarc_object_detection/coco_train_large.json')

    @property
    def valid_annotation_path(self):
        return os.path.join('/mnt/nas7/users/guowen/aarc_object_detection/coco_train_small.json')

    @property
    def test_annotation_path(self):
        return os.path.join(self.save_path, 'annotations/instances_val2017.json')



    #@property
    #def normalize(self):
    #    return transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])

    #def build_train_transform(self, distort_color, resize_scale):
    #    print('Color jitter: %s' % distort_color)
    #    if distort_color == 'strong':
    #        color_transform = transforms.ColorJitter(brightness=0.4, contrast=0.4, saturation=0.4, hue=0.1)
    #    elif distort_color == 'normal':
    #        color_transform = transforms.ColorJitter(brightness=32. / 255., saturation=0.5)
    #    else:
    #        color_transform = None
    #    if color_transform is None:
    #        train_transforms = transforms.Compose([
    #            transforms.RandomResizedCrop(self.image_size, scale=(resize_scale, 1.0)),
    #            transforms.RandomHorizontalFlip(),
    #            transforms.ToTensor(),
    #            self.normalize,
    #        ])
    #    else:
    #        train_transforms = transforms.Compose([
    #            transforms.RandomResizedCrop(self.image_size, scale=(resize_scale, 1.0)),
    #            transforms.RandomHorizontalFlip(),
    #            color_transform,
    #            transforms.ToTensor(),
    #            self.normalize,
    #        ])
    #   return train_transforms

    #@property
    #def resize_value(self):
    #    return 256

    @property
    def image_size(self):
        return 667

class COCODataPrefetcher():
    def __init__(self, loader):
        self.loader = iter(loader)
        self.stream = torch.cuda.Stream()
        self.preload()

    def preload(self):
        try:
            sample = next(self.loader)
            self.next_input, self.next_annot = sample['img'], sample['annot']
        except StopIteration:
            self.next_input = None
            self.next_annot = None
            return
        with torch.cuda.stream(self.stream):
            self.next_input = self.next_input.cuda(non_blocking=True)
            self.next_annot = self.next_annot.cuda(non_blocking=True)
            self.next_input = self.next_input.float()

    def next(self):
        torch.cuda.current_stream().wait_stream(self.stream)
        input = self.next_input
        annot = self.next_annot
        self.preload()
        return input, annot


def collater(data):
    """
    image and annotation shape should be the same
    """
    imgs = [s['img'] for s in data]
    annots = [s['annot'] for s in data]
    scales = [s['scale'] for s in data]


    imgs = torch.from_numpy(np.stack(imgs, axis=0))
    # the max num annotation in an image
    max_num_annots = max(annot.shape[0] for annot in annots)
    #if the num of annotations in an image is less than the max value padding with -1
    if max_num_annots > 0:

        annot_padded = torch.ones((len(annots), max_num_annots, 5)) * (-1)

        if max_num_annots > 0:
            for idx, annot in enumerate(annots):
                if annot.shape[0] > 0:
                    annot_padded[idx, :annot.shape[0], :] = annot
    else:
        annot_padded = torch.ones((len(annots), 1, 5)) * (-1)

    imgs = imgs.permute(0, 3, 1, 2)

    return {'img': imgs, 'annot': annot_padded, 'scale': scales}


class RandomFlip(object):
    def __init__(self, flip_prob=0.5):
        self.flip_prob = flip_prob

    def __call__(self, sample):
        #if sample['img'] is not None:
        if np.random.uniform(0, 1) < self.flip_prob:
            #print(type(sample))
            image, annots, scale = sample['img'], sample['annot'], sample[
                'scale']
            image = image[:, ::-1, :]

            _, width, _ = image.shape

            x1 = annots[:, 0].copy()
            x2 = annots[:, 2].copy()

            annots[:, 0] = width - x2
            annots[:, 2] = width - x1

            sample = {'img': image, 'annot': annots, 'scale': scale}

        return sample


class RandomCrop(object):
    def __init__(self, crop_prob=0.5):
        self.crop_prob = crop_prob

    def __call__(self, sample):
        image, annots, scale = sample['img'], sample['annot'], sample['scale']

        if annots.shape[0] == 0:
            return sample

        if np.random.uniform(0, 1) < self.crop_prob:
            h, w, _ = image.shape
            max_bbox = np.concatenate([
                np.min(annots[:, 0:2], axis=0),
                np.max(annots[:, 2:4], axis=0)
            ],
                                      axis=-1)
            max_left_trans, max_up_trans = max_bbox[0], max_bbox[1]
            max_right_trans, max_down_trans = w - max_bbox[2], h - max_bbox[3]
            crop_xmin = max(
                0, int(max_bbox[0] - random.uniform(0, max_left_trans)))
            crop_ymin = max(0,
                            int(max_bbox[1] - random.uniform(0, max_up_trans)))
            crop_xmax = max(
                w, int(max_bbox[2] + random.uniform(0, max_right_trans)))
            crop_ymax = max(
                h, int(max_bbox[3] + random.uniform(0, max_down_trans)))

            image = image[crop_ymin:crop_ymax, crop_xmin:crop_xmax]
            annots[:, [0, 2]] = annots[:, [0, 2]] - crop_xmin
            annots[:, [1, 3]] = annots[:, [1, 3]] - crop_ymin

            sample = {'img': image, 'annot': annots, 'scale': scale}

        return sample


class RandomTranslate(object):
    def __init__(self, translate_prob=0.5):
        self.translate_prob = translate_prob

    def __call__(self, sample):
        image, annots, scale = sample['img'], sample['annot'], sample['scale']

        if annots.shape[0] == 0:
            return sample

        if np.random.uniform(0, 1) < self.translate_prob:
            h, w, _ = image.shape
            max_bbox = np.concatenate([
                np.min(annots[:, 0:2], axis=0),
                np.max(annots[:, 2:4], axis=0)
            ],
                                      axis=-1)
            max_left_trans, max_up_trans = max_bbox[0], max_bbox[1]
            max_right_trans, max_down_trans = w - max_bbox[2], h - max_bbox[3]
            tx = random.uniform(-(max_left_trans - 1), (max_right_trans - 1))
            ty = random.uniform(-(max_up_trans - 1), (max_down_trans - 1))
            M = np.array([[1, 0, tx], [0, 1, ty]])
            image = cv2.warpAffine(image, M, (w, h))
            annots[:, [0, 2]] = annots[:, [0, 2]] + tx
            annots[:, [1, 3]] = annots[:, [1, 3]] + ty

            sample = {'img': image, 'annot': annots, 'scale': scale}

        return sample


class Resize(object):
    def __init__(self, resize=600):
        self.resize = resize

    def __call__(self, sample):
        image, annots, scale = sample['img'], sample['annot'], sample['scale']
        height, width, _ = image.shape

        max_image_size = max(height, width)
        resize_factor = self.resize / max_image_size
        resize_height, resize_width = int(height * resize_factor), int(
            width * resize_factor)

        image = cv2.resize(image, (resize_width, resize_height))

        new_image = np.zeros((self.resize, self.resize, 3))
        new_image[0:resize_height, 0:resize_width] = image

        annots[:, :4] *= resize_factor
        scale = scale * resize_factor

        return {
            'img': torch.from_numpy(new_image),
            'annot': torch.from_numpy(annots),
            'scale': scale
        }


if __name__ == '__main__':
    import torchvision.transforms as transforms
    from tqdm import tqdm
    coco = CocoDetection(
        image_root_dir=
        '/home/zgcr/Downloads/datasets/COCO2017/images/train2017/',
        annotation_root_dir=
        "/home/zgcr/Downloads/datasets/COCO2017/annotations/",
        set='train2017',
        transform=transforms.Compose([
            RandomFlip(),
            Resize(resize=600),
        ]))

    print(len(coco))
    print(coco.category_id_to_coco_label)

    print(coco[0]['img'].shape, coco[0]['annot'], coco[0]['scale'])

    # retinanet resize method
    # resize=400,per_image_average_area=223743,input shape=[667,667]
    # resize=500,per_image_average_area=347964,input shape=[833,833]
    # resize=600,per_image_average_area=502820,input shape=[1000,1000]
    # resize=700,per_image_average_area=682333,input shape=[1166,1166]
    # resize=800,per_image_average_area=891169,input shape=[1333,1333]

    # my resize method
    # resize=600,per_image_average_area=258182,input shape=[600,600]
    # resize=667,per_image_average_area=318986,input shape=[667,667]
    # resize=700,per_image_average_area=351427,input shape=[700,700]
    # resize=800,per_image_average_area=459021,input shape=[800,800]
    # resize=833,per_image_average_area=497426,input shape=[833,833]
    # resize=900,per_image_average_area=580988,input shape=[900,900]
    # resize=1000,per_image_average_area=717349,input shape=[1000,1000]
    # resize=1166,per_image_average_area=974939,input shape=[1166,1166]
    # resize=1333,per_image_average_area=1274284,input shape=[1333,1333]