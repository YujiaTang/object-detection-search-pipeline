# ProxylessNAS: Direct Neural Architecture Search on Target Task and Hardware
# Han Cai, Ligeng Zhu, Song Han
# International Conference on Learning Representations (ICLR), 2019.

import yaml
import os
import sys
import pickle
try:
    from urllib import urlretrieve
except ImportError:
    from urllib.request import urlretrieve


def download_url(url, model_dir='~/.torch/proxyless_nas', overwrite=False):
    target_dir = url.split('//')[-1]
    target_dir = os.path.dirname(target_dir)
    model_dir = os.path.expanduser(model_dir)
    model_dir = os.path.join(model_dir, target_dir)
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    filename = url.split('/')[-1]
    cached_file = os.path.join(model_dir, filename)
    if not os.path.exists(cached_file) or overwrite:
        sys.stderr.write('Downloading: "{}" to {}\n'.format(url, cached_file))
        urlretrieve(url, cached_file)
    return cached_file


class LatencyEstimator1(object):
    def __init__(self, url='https://file.lzhu.me/projects/proxylessNAS/LatencyTools/mobile_trim.yaml'):
        fname = download_url(url, overwrite=True)

        with open(fname, 'r') as fp:
            self.lut = yaml.load(fp)

    @staticmethod
    def repr_shape(shape):
        if isinstance(shape, (list, tuple)):
            return 'x'.join(str(_) for _ in shape)
        elif isinstance(shape, str):
            return shape
        else:
            return TypeError

    def predict(self, ltype: str, _input, output, expand=None, kernel=None, stride=None, idskip=None, ):
        """
        :param ltype:
            Layer type must be one of the followings
                1. `Conv`: The initial 3x3 conv with stride 2.
                2. `Conv_1`: The upsample 1x1 conv that increases num_filters by 4 times.
                3. `Logits`: All operations after `Conv_1`.
                4. `expanded_conv`: MobileInvertedResidual
        :param _input: input shape (h, w, #channels)
        :param output: output shape (h, w, #channels)
        :param expand: expansion ratio
        :param kernel: kernel size
        :param stride:
        :param idskip: indicate whether has the residual connection
        """
        infos = [ltype, 'input:%s' % self.repr_shape(_input), 'output:%s' % self.repr_shape(output), ]

        if ltype in ('expanded_conv',):
            assert None not in (expand, kernel, stride, idskip)
            infos += ['expand:%d' % expand, 'kernel:%d' % kernel, 'stride:%d' % stride, 'idskip:%d' % idskip]
        key = '-'.join(infos)
        return self.lut[key]['mean']


class LatencyEstimator(object):
    def __init__(self, hardware, file_path1='/mnt/nas1/users/guowen/aarc_face/latency_lookup_table/result/V100_ResConv/v100_ResConv_merge_batch8_corr.pkl',
                 file_path3='/mnt/nas1/users/guowen/aarc_face/latency_lookup_table/result/Mate30_ResConv/mate30_ResConv_merge_batch1_corr.pkl',
                 file_path4='/mnt/nas1/users/guowen/aarc_face/latency_lookup_table/result/Jetson_ResConv/jetson_ResConv_latency_batch4_corr.pkl',
                 file_path2='/mnt/nas1/users/guowen/aarc_face/latency_lookup_table/result/Jetson_ResConv/jetson_fpn_head_latency_batch4.pkl'):
        if hardware=='v100':
            file_path=file_path1
        if hardware=='mobile':
            file_path=file_path3
        if hardware=='jetson':
            file_path=file_path4
        print('latency file1:', file_path)
        print('latency file2:', file_path2)
        pickle_in1 = open(file_path, "rb")
        pickle_in2 = open(file_path2, "rb")
        self.lat_dic = pickle.load(pickle_in1)
        self.lat_fpn_dic = pickle.load(pickle_in2)

    @staticmethod
    def repr_shape(shape):
        if isinstance(shape, (list, tuple)):
            return 'x'.join(str(_) for _ in shape)
        elif isinstance(shape, str):
            return shape
        else:
            return TypeError

    def predict(self, ltype: str, _input=None, output=None, expand=None, kernel=None, stride=None, idskip=None, groups=None, act=None, type=None, image_size=None):
        """
        :param ltype:
            Layer type must be one of the followings
                1. `Conv`: The initial 3x3 conv with stride 2.
                2. `Conv_1`: The upsample 1x1 conv that increases num_filters by 4 times.
                3. `Logits`: All operations after `Conv_1`.
                4. `expanded_conv`: MobileInvertedResidual
        :param _input: input shape (h, w, #channels)
        :param output: output shape (h, w, #channels)
        :param expand: expansion ratio
        :param kernel: kernel size
        :param stride:
        :param idskip: indicate whether has the residual connection
        """
        if output is not None:
            infos = [ltype, 'input:%s' % self.repr_shape(_input), 'output:%s' % self.repr_shape(output), ]

        if ltype in ('ResConv',):
            assert None not in (expand, kernel, stride, idskip)
            infos += ['expand:%d' % expand, 'kernel:%d' % kernel, 'stride:%d' % stride, 'idskip:%d' % idskip]
            key = '-'.join(infos)
        elif ltype in ('Conv', ):
            assert None not in (kernel, stride, groups, act)
            infos += ['kernel:%d' % kernel, 'stride:%d' % stride, 'groups:%d' % groups, 'act:'+act]
            key = '-'.join(infos)
        elif ltype in ('Pooling',):
            assert None not in (kernel, stride, type)
            infos = [ltype, 'input:%s' % self.repr_shape(_input), ]
            infos += ['kernel:%d' % kernel, 'stride:%d' % stride, 'type:' + type]
            #infos += ['type:avg']
            key = '-'.join(infos)
        elif ltype in ('fpn',):
            assert image_size is not None
            #print('ltype', ltype)
            return self.lat_fpn_dic[image_size][ltype]['mean']
        elif ltype in ('cls_head',):
            assert image_size is not None
            #print('ltype', ltype)
            return self.lat_fpn_dic[image_size][ltype]['mean']
        elif ltype in ('reg_head',):
            assert image_size is not None
            #print('ltype', ltype)
            return self.lat_fpn_dic[image_size][ltype]['mean']

        #print('key:', key)
        if key in self.lat_dic:
            return self.lat_dic[key]['mean']
        else:
            print('key:', key, ' is not in latency dictionary, return 4.0')
            return 4.0

if __name__ == '__main__':
    est = LatencyEstimator()
    s = est.predict('FAConv', _input=(64, 56, 56), output=(64, 28, 28), expand=6, kernel=3, stride=2, idskip=0, groups=512, act='linear')
    print(s)
