import torch
import sys

supernet = torch.load("/mnt/nas7/users/tangyujia/code/proxylessnas-detection/search/"
                      "search-800-refvalue-600/Exp/test_res_recognition/checkpoint/warmup_best.pth.tar")
selectednet = torch.load("/mnt/nas4/users/tangyujia/code/mmdetection/work_dirs/retinanet_search_mobile_55000_fpn_1x/epoch_1.pth")

#file = open('','w+')
#for key in supernet['state_dict']:
#    file.write('%s\n' %key)
#file.close()
#modify the file, delete unused layers and change the order of shortcut layers

file = open("/mnt/nas4/users/tangyujia/code/mmdetection/work_dirs/retinanet_search_mobile_55000_fpn_1x/selected_layers.txt",'r')
res = []
for line in file:
    res.append(line)

res = [a.strip('\n') for a in res]

for ith, key in enumerate(selectednet['state_dict']):
    #if selectednet['state_dict'][key].shape==supernet['state_dict'][res[ith]].shape:
    selectednet['state_dict'][key] = supernet['state_dict'][res[ith]]
    #else:
    #   print('layer name:', key,selectednet['state_dict'][key].shape,supernet['state_dict'][res[ith]].shape)
    #    raise ValueError('do not match')

print('reg_head.output.bias', supernet['state_dict']['reg_head.output.bias'])
print('bbox_head.retina_reg.bias', selectednet['state_dict']['bbox_head.retina_reg.bias'])

selectednet['state_dict']['neck.lateral_convs.0.conv.weight'] = supernet['state_dict']['fpn.P3_1.weight']
selectednet['state_dict']['neck.lateral_convs.0.conv.bias'] = supernet['state_dict']['fpn.P3_1.bias']
selectednet['state_dict']['neck.lateral_convs.1.conv.weight'] = supernet['state_dict']['fpn.P4_1.weight']
selectednet['state_dict']['neck.lateral_convs.1.conv.bias'] = supernet['state_dict']['fpn.P4_1.bias']
selectednet['state_dict']['neck.lateral_convs.2.conv.weight'] = supernet['state_dict']['fpn.P5_1.weight']
selectednet['state_dict']['neck.lateral_convs.2.conv.bias'] = supernet['state_dict']['fpn.P5_1.bias']
selectednet['state_dict']['neck.fpn_convs.0.conv.weight'] = supernet['state_dict']['fpn.P3_2.weight']
selectednet['state_dict']['neck.fpn_convs.0.conv.bias'] = supernet['state_dict']['fpn.P3_2.bias']
selectednet['state_dict']['neck.fpn_convs.1.conv.weight'] = supernet['state_dict']['fpn.P4_2.weight']
selectednet['state_dict']['neck.fpn_convs.1.conv.bias'] = supernet['state_dict']['fpn.P4_2.bias']
selectednet['state_dict']['neck.fpn_convs.2.conv.weight'] = supernet['state_dict']['fpn.P5_2.weight']
selectednet['state_dict']['neck.fpn_convs.2.conv.bias'] = supernet['state_dict']['fpn.P5_2.bias']
selectednet['state_dict']['neck.fpn_convs.3.conv.weight'] = supernet['state_dict']['fpn.P6.weight']
selectednet['state_dict']['neck.fpn_convs.3.conv.bias'] = supernet['state_dict']['fpn.P6.bias']
selectednet['state_dict']['neck.fpn_convs.4.conv.weight'] = supernet['state_dict']['fpn.P7_2.weight']
selectednet['state_dict']['neck.fpn_convs.4.conv.bias'] = supernet['state_dict']['fpn.P7_2.bias']
selectednet['state_dict']['bbox_head.cls_convs.0.conv.weight'] = supernet['state_dict']['cls_head.cls_head.0.weight']
selectednet['state_dict']['bbox_head.cls_convs.0.conv.bias'] = supernet['state_dict']['cls_head.cls_head.0.bias']
selectednet['state_dict']['bbox_head.cls_convs.1.conv.weight'] = supernet['state_dict']['cls_head.cls_head.2.weight']
selectednet['state_dict']['bbox_head.cls_convs.1.conv.bias'] = supernet['state_dict']['cls_head.cls_head.2.bias']
selectednet['state_dict']['bbox_head.cls_convs.2.conv.weight'] = supernet['state_dict']['cls_head.cls_head.4.weight']
selectednet['state_dict']['bbox_head.cls_convs.2.conv.bias'] = supernet['state_dict']['cls_head.cls_head.4.bias']
selectednet['state_dict']['bbox_head.cls_convs.3.conv.weight'] = supernet['state_dict']['cls_head.cls_head.6.weight']
selectednet['state_dict']['bbox_head.cls_convs.3.conv.bias'] = supernet['state_dict']['cls_head.cls_head.6.bias']
selectednet['state_dict']['bbox_head.reg_convs.0.conv.weight'] = supernet['state_dict']['reg_head.reg_head.0.weight']
selectednet['state_dict']['bbox_head.reg_convs.0.conv.bias'] = supernet['state_dict']['reg_head.reg_head.0.bias']
selectednet['state_dict']['bbox_head.reg_convs.1.conv.weight'] = supernet['state_dict']['reg_head.reg_head.2.weight']
selectednet['state_dict']['bbox_head.reg_convs.1.conv.bias'] = supernet['state_dict']['reg_head.reg_head.2.bias']
selectednet['state_dict']['bbox_head.reg_convs.2.conv.weight'] = supernet['state_dict']['reg_head.reg_head.4.weight']
selectednet['state_dict']['bbox_head.reg_convs.2.conv.bias'] = supernet['state_dict']['reg_head.reg_head.4.bias']
selectednet['state_dict']['bbox_head.reg_convs.3.conv.weight'] = supernet['state_dict']['reg_head.reg_head.6.weight']
selectednet['state_dict']['bbox_head.reg_convs.3.conv.bias'] = supernet['state_dict']['reg_head.reg_head.6.bias']
selectednet['state_dict']['bbox_head.retina_cls.weight'] = supernet['state_dict']['cls_head.output.weight']
selectednet['state_dict']['bbox_head.retina_cls.bias'] = supernet['state_dict']['cls_head.output.bias']
selectednet['state_dict']['bbox_head.retina_reg.weight'] = supernet['state_dict']['reg_head.output.weight']
selectednet['state_dict']['bbox_head.retina_reg.bias'] = supernet['state_dict']['reg_head.output.bias']


torch.save(selectednet, "/mnt/nas4/users/tangyujia/code/mmdetection/work_dirs/retinanet_search_mobile_55000_fpn_1x/weight_transfer.pth")


